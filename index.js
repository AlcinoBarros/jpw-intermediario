const express = require('express');
const app = express();
const porta = process.env.PORT || 3000;
const routes = require('./api/routes');
const mongoose = require('./api/data');
const cors = require('cors');
//const url = require('./api/data/index');

//url(process.env.MONGODB_URI);

const url =
  'mongodb+srv://jpw:123@cluster0.nxoxt.mongodb.net/myFirstDatabase?retryWrites=true&w=majority';

app.use(cors());
app.use(express.json()); //tratar todo corpo de mensagem JSON
app.use('/api', routes); //cors todo corpo de mensagem JSON

mongoose.connect(url, { useNewUrlParser: true }).then(function () {
  app.listen(process.env.PORT || 8080, function () {
    console.log(`Servidor rodando na porta ${porta}`);
  });
});
