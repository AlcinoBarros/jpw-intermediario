const mongoose = require('../data');

let professorSchema = new mongoose.Schema({
  documento: {
    type: String,
    required: true,
  },
  nome: {
    type: String,
    required: true,
  },
  telefone: String,
  email: String,
  disciplina: String
}, {timestamps: true});

let Professor = mongoose.model('Professor', professorSchema)
module.exports = Professor;