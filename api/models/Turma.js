const { Schema } = require('../data');
const mongoose = require('../data');

let turmaSchema = new mongoose.Schema(
  {
    nome: {
      type: String,
      required: true,
    },
    professor: {
      type: String,
      required: true,
        
      
    },
    aluno: {
      type: String,
      required: true,
    },
  },
  { timestamps: true }
);

let Turma = mongoose.model('Turma', turmaSchema);
module.exports = Turma;
