const express = require('express');
const router = express.Router();
const Professor = require('../models/Professor');

// mostrar todos os professores
router.get('/', async (req, res) => {
  let nLimit = parseInt(req.query.limit) || 10;
  let nSkip = parseInt(req.query.skip) || 0;
  const profs = await Professor.find().limit(nLimit).skip(nSkip);
  res.status(200).json(profs);
});

// mostrar um professor
router.get('/:id', async (req, res, next) => {
  try {
    const id = req.params.id;
    var profs = await Professor.findById(id);
    if (!profs)
      return res.status(404).json({
        erro: 'Professor não encontrado',
      });
    res.status(200).json(profs);
  } catch (err) {
    next(err);
  }
});

// Inserir um novo professor
router.post('/', async (req, res) => {
  const prof = new Professor(req.body);
  var resultado = await prof.save();
  return res.status(200).json(resultado);
});

// Atualizar um professor
router.put('/:id', async (req, res) => {
  const id = req.params.id;
  const novoProfessor = req.body;
  const atualProfessor = await Professor.findByIdAndUpdate(id, novoProfessor, {
    new: true,
  });
  return res.status(200).json(atualProfessor);
});

// Deletar professor
router.delete('/:id', async (req, res) => {
  const id = req.params.id;
  const prof = await Professor.findByIdAndDelete(id);
  res.status(200).json(prof);
});

module.exports = router;
